const alumnos = [   
    {
       "cod_carrera": "60",
       "legajo": "10754",
       "apellido": "Peña",
       "nombre": "Hellen"
    },
    {
       "cod_carrera": "60",
       "legajo": "11282",
       "apellido": "Vega",
       "nombre": "Michel"
    },
    {
       "cod_carrera": "65",
       "legajo": "11016",
       "apellido": "Toro",
       "nombre": "Marthyna"
    }
 ];

 const altaAlumno = nuevoAlumno => {      
      alumnos.push(nuevoAlumno);
      console.log('Alta exitosa!');   
};

const alumnosPorNombre = nombre => {
      alumnos.filter(nom => {nom.nombre = nombre} );
};

//la referencia a la funcion (sin parentesis) se utiliza para
//exportarla como objeto para utilizar después
//si la pasara con parentesis sería una invocación (llamada) 
//y no es lo que se busca
 module.exports = {alumnos, altaAlumno, alumnosPorNombre};