const express = require("express");
const hbs = require("hbs");

const app = express();

// Corre el archivo helpers.js
require(`${__dirname}/views/helpers/helpers`)
// Obtener alumnos
const {
    alumnos,
    altaAlumno,
    alumnosPorNombre
} = require(`${__dirname}/data/alumnos`);

// Habilita la carpeta public para servir contenido estático
app.use(express.static(`${__dirname}/public`));

// Middleware para parsear el cuerpo de las peticiones HTTP
app.use(express.urlencoded({
    extended: true
}))

// Establece motor de plantillas
app.set('view engine', 'hbs');
// Registrar dónde están las vistas parciales
hbs.registerPartials(`${__dirname}/views/partials`)


//el la ruta principal
app.get('/', (req, res) => {
    //muestra plantilla 'home'
    res.render('home', {
        //Le da un valor a la propiedad {{nombreDeLaApp}} 
        //en la plantilla 'home.hbs'
        nombreDeLaApp: "Instituto Fake"
    });
})

app.get('/alumnos', (req, res) => {
    /*res.render('alumnos', {
        alumnos: alumnos
    });*/ // ES5

    //muestra la plantilla alumnos con el objeto 'alumnos' que
    //se importó
    res.render('alumnos', {
        alumnos,
    }); // ES6
})

app.get('/contacto', (req, res) => {
    //muestra la plantilla 'contacto'
    res.render('contacto');
})

//petición gráfica para el cliente (navegador) (no hay parametros)
app.get('/agregarAlumno', (req, res) => {
    //muestra la plantilla 'agregarAlumno'
    res.render('agregarAlumno'); 
})

//petition post para agregar recurso al servidor
app.post('/agregarAlumno', (req, res) => {         
    try {
        const { //extraigo del body los campos que me interesan (todos) de la petición post
            codigo,  //si voy a mandar por formulario, este campo debe coincidir con la propiedad "name" del input
            legajo,  //idem
            apellido, //idem
            nombre    //idem
        } = req.body; 
        altaAlumno({ //creo un nuevo objeto alumno con los campos extraidos 
            "cod_carrera": codigo,
            "legajo": legajo,
            "apellido": apellido,
            "nombre": nombre
        });
        console.log(alumnos);
        res.send('Alta exitosa!');        
    } catch (error) {
        res.send(error);
    }
});

//solicitud recurso grafico (ver pagina) para el cliente (navegador)
app.get('/alumnosPorNombre', (req, res) => {
    try{
        const {nombre2} = req.body;
        const alumnosByName = alumnosPorNombre(nombre2);
        res.render('alumnosPorNombre', { alumnosByName });
    }catch(error){
        res.send('No hay alumnos con ese nombre');
    }
});

//solicitud con datos formulario para el servidor (POST)
//app.post('/alumnosPorNombre', (req, res) => {
//    alumnosPorNombre()
//});

//definicion de ruta estandar para las no definidas
app.get('*', (req, res) => {
    //muestra plantilla 404 en las rutas no especificadas
    res.render('404');
})

//inicializa el servidor
app.listen(8080, () => {
    console.log("Escuchando peticiones en 8080...");
})