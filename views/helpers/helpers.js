//es codigo que se va a actualizar automaticamente sin tener que 
//reescribirlo en cada plantilla
const hbs = require('hbs'); //requiere el modulo 'hbs'

//registra el helper
hbs.registerHelper('getAnioActual', () => new Date().getFullYear());